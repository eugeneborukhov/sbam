import React from "react";
import logo from "./spam-classic.jpg";
import VerticalLineStepper from "./VerticalLineStepper";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import "./App.css";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#55a3d1"
    },
    secondary: {
      main: "#E33E7F"
    }
  }
});

function App() {
  return (
    <>
      <MuiThemeProvider theme={theme}>
        <div style={{ backgroundColor: "#F3F3F4", height: "100vh" }}>
          <div className="App">
            <header className="App-header">
              <span style={{ padding: "10px", fontFamily: "Arial" }}>
                Simply Better Account Management
              </span>
              <img
                src={logo}
                className="App-logo"
                style={{ display: "inline-block", padding: "5px" }}
              ></img>
            </header>
          </div>
          <div style={{ marginTop: "5px" }}>
            <VerticalLineStepper />
          </div>
        </div>
      </MuiThemeProvider>
    </>
  );
}

export default App;
