import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { Select, MenuItem, FormControl } from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles(theme => ({
  root: {
    width: "50%",
    margin: "0 auto"
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  }
}));

function getSteps() {
  return [
    "Panels",
    "Sites",
    "Doors",
    "Groups",
    "Schedules",
    "Associations",
    "Review"
  ];
}

export default function VerticalLinearStepper() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const [accountName, setAccountName] = useState(null);
  const [controlPanels, setControlPanels] = useState([null]);
  const [sites, setSites] = useState([{}]);
  const [doors, setDoors] = useState([{}]);
  const [groups, setGroups] = useState([null]);
  const [schedules, setSchedules] = useState([{}]);

  const onHandleAccountNameChange = e => {
    setAccountName(e.target.value);
  };

  const onHandleControlPanelChange = (e, index) => {
    const newControlPanels = [...controlPanels];
    newControlPanels[index] = e.target.value;
    setControlPanels(newControlPanels);
  };

  const onHandleSiteNameChange = (e, index) => {
    const newSites = [...sites];
    newSites[index].siteName = e.target.value;
    setSites(newSites);
  };

  const onHandleAddress1Change = (e, index) => {
    const newSites = [...sites];
    newSites[index].address1 = e.target.value;
    setSites(newSites);
  };

  const onHandleAddress2Change = (e, index) => {
    const newSites = [...sites];
    newSites[index].address2 = e.target.value;
    setSites(newSites);
  };

  const onHandleCityChange = (e, index) => {
    const newSites = [...sites];
    newSites[index].city = e.target.value;
    setSites(newSites);
  };

  const onHandleStateChange = (e, index) => {
    const newSites = [...sites];
    newSites[index].state = e.target.value;
    setSites(newSites);
  };

  const onHandleZipCodeChange = (e, index) => {
    const newSites = [...sites];
    newSites[index].zipCode = e.target.value;
    setSites(newSites);
  };

  const onHandleDoorNameChange = (e, index) => {
    const newDoors = [...doors];
    newDoors[index].doorName = e.target.value;
    setDoors(newDoors);
  };

  const onHandleDoorControlPanelChange = (e, index) => {
    const newDoors = [...doors];
    newDoors[index].controlPanel = e.target.value;
    setDoors(newDoors);
  };

  const onHandleDoorSiteChange = (e, index) => {
    const newDoors = [...doors];
    newDoors[index].site = e.target.value;
    setDoors(newDoors);
  };

  const onHandleDoorGroupChange = (e, index) => {
    const newDoors = [...doors];
    newDoors[index].group = e.target.value;
    setDoors(newDoors);
  };

  const onHandleDoorScheduleChange = (e, index) => {
    const newDoors = [...doors];
    newDoors[index].schedule = e.target.value;
    setDoors(newDoors);
  };

  const onHandleGroupChange = (e, index) => {
    const newGroups = [...groups];
    newGroups[index] = e.target.value;
    setGroups(newGroups);
  };

  const onHandleStartTimeChange = (e, index) => {
    const newSchedules = [...schedules];
    newSchedules[index].startTime = e.target.value;
    setSchedules(newSchedules);
  };

  const onHandleEndTimeChange = (e, index) => {
    const newSchedules = [...schedules];
    newSchedules[index].endTime = e.target.value;
    setSchedules(newSchedules);
  };

  const onHandleDaysChange = (e, index) => {
    const newSchedules = [...schedules];
    newSchedules[index].days = e.target.value;
    setSchedules(newSchedules);
  };

  const addControlPanel = () => {
    setControlPanels([...controlPanels, null]);
  };

  const addSite = () => {
    setSites([...sites, {}]);
  };

  const addDoor = () => {
    setDoors([...doors, {}]);
  };

  const addGroup = () => {
    setGroups([...groups, null]);
  };

  const addSchedule = () => {
    setSchedules([...schedules, {}]);
  };

  const submitForm = async () => {
    console.log("submitting");
    const url = "http://localhost:8086/api/accounts/create-with-doors";

    const data = {
      accountName: accountName,
      sites: sites,
      doors: doors
    };

    try {
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X29iamVjdF9pZCI6NzM5MzIwLCJ1c2VyX2lkIjoiNzM5MzIxIiwidXNlcl9uYW1lIjoiYXBpYWRtaW4iLCJzY29wZSI6WyJicml2by5hcGkiXSwiaXNzdWVkIjoiMTU3NjI1NTExOTk4NyIsImV4cCI6MTY3NjI1NTExOCwic2VydmljZV90b2tlbiI6bnVsbCwiYXV0aG9yaXRpZXMiOlsiUk9MRV9NQVNURVJfQURNSU4iLCJST0xFX0FETUlOIl0sImp0aSI6ImE1OTFmNjdhLTE4NGYtNDViMi1hN2VjLTgzOTAzNjMxNDliZiIsImNsaWVudF9pZCI6ImE3YmVkMGE3LTgwYjEtNDU4Mi04YmMxLTFiNmJjZGU3YjU2ZCIsIndoaXRlX2xpc3RlZCI6ZmFsc2V9.LVZzTHVm4guxDFXAGtSXagl4ujSbWDcLe6VP_l3J-86G1ONdI6Xxfgk8hGecZEUTtwvoprNKe3Ijo-nqYhNLxkc-PPfpRyd5orJnWUAGsPMhFLp7vR9xr6mEd3mkRAVgGnM9BfNRpv1KzGePdDl5-h6GkibjaOcZXWrT7l2_HyI`
        }
      });
      const json = await response.json();
      console.log("Success:", JSON.stringify(json));
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
    if (activeStep === steps.length - 1) {
      submitForm();
    }
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setAccountName(null);
    setControlPanels([null]);
    setSites([{}]);
    setDoors([{}]);
    setGroups([null]);
    setSchedules([{}]);
    setActiveStep(0);
  };

  const getStepContent = step => {
    switch (step) {
      case 0:
        return (
          <>
            <div>
              <TextField
                id="standard-basic"
                label="Account Name"
                value={accountName}
                onChange={onHandleAccountNameChange}
              />
            </div>
            {controlPanels.map((val, index) => {
              return (
                <div>
                  <TextField
                    id="standard-basic"
                    label="Control Panel #"
                    value={val}
                    onChange={e => onHandleControlPanelChange(e, index)}
                    style={{ marginTop: "10px" }}
                  />
                </div>
              );
            })}
            <Button
              onClick={addControlPanel}
              className={classes.button}
              color="primary"
            >
              Add Control Panel
            </Button>
          </>
        );
      case 1:
        return (
          <>
            {sites.map((val, index) => {
              return (
                <>
                  <div>
                    <TextField
                      id="standard-basic"
                      label="Site Name"
                      value={val.siteName}
                      onChange={e => onHandleSiteNameChange(e, index)}
                    />
                  </div>
                  <div>
                    <TextField
                      id="standard-basic"
                      label="Address 1"
                      value={val.address1}
                      onChange={e => onHandleAddress1Change(e, index)}
                    />{" "}
                    <TextField
                      id="standard-basic"
                      label="Address 2"
                      value={val.address2}
                      onChange={e => onHandleAddress2Change(e, index)}
                    />
                  </div>
                  <div>
                    <TextField
                      id="standard-basic"
                      label="City"
                      value={val.city}
                      onChange={e => onHandleCityChange(e, index)}
                    />{" "}
                    <TextField
                      id="standard-basic"
                      label="State"
                      value={val.state}
                      onChange={e => onHandleStateChange(e, index)}
                    />{" "}
                    <TextField
                      id="standard-basic"
                      label="Zip Code"
                      value={val.zipCode}
                      onChange={e => onHandleZipCodeChange(e, index)}
                    />
                  </div>
                </>
              );
            })}
            <Button
              onClick={addSite}
              className={classes.button}
              color="primary"
            >
              Add Site
            </Button>
          </>
        );
      case 2:
        return (
          <>
            {doors.map((value, index) => {
              return (
                <div style={{ display: "flex" }}>
                  <FormControl>
                    <InputLabel id="control-panel-id">Control Panel</InputLabel>
                    <Select
                      labelId="control-panel-id"
                      margin="none"
                      style={{ width: "10rem", marginRight: "10px" }}
                      value={value.controlPanel}
                      onChange={e => onHandleDoorControlPanelChange(e, index)}
                    >
                      {controlPanels.map(controlPanel => (
                        <MenuItem value={controlPanel}>{controlPanel}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl>
                    <InputLabel id="site-id">Site</InputLabel>
                    <Select
                      labelId="site-id"
                      margin="none"
                      style={{ width: "10rem", marginRight: "10px" }}
                      value={value.site}
                      onChange={e => onHandleDoorSiteChange(e, index)}
                    >
                      {sites.map(site => (
                        <MenuItem value={site.siteName}>
                          {site.siteName}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>{" "}
                  <TextField
                    id="standard-basic"
                    label="Door Name"
                    margin="none"
                    value={value.doorName}
                    onChange={e => onHandleDoorNameChange(e, index)}
                  />
                </div>
              );
            })}

            <Button
              onClick={addDoor}
              className={classes.button}
              color="primary"
            >
              Add Door
            </Button>
          </>
        );
      case 3:
        return (
          <>
            {groups.map((value, index) => {
              return (
                <div>
                  <TextField
                    id="standard-basic"
                    label="Group Name"
                    value={value}
                    onChange={e => onHandleGroupChange(e, index)}
                    style={{ marginTop: "10px" }}
                  />
                </div>
              );
            })}
            <Button
              onClick={addGroup}
              className={classes.button}
              color="primary"
            >
              Add Group
            </Button>
          </>
        );
      case 4:
        return (
          <>
            {schedules.map((value, index) => {
              return (
                <div>
                  <TextField
                    id="standard-basic"
                    label="Start Time"
                    value={value.startTime}
                    onChange={e => onHandleStartTimeChange(e, index)}
                    style={{ marginTop: "10px" }}
                  />{" "}
                  <TextField
                    id="standard-basic"
                    label="End Time"
                    value={value.endTime}
                    onChange={e => onHandleEndTimeChange(e, index)}
                    style={{ marginTop: "10px" }}
                  />{" "}
                  <TextField
                    id="standard-basic"
                    label="Days"
                    value={value.days}
                    onChange={e => onHandleDaysChange(e, index)}
                    style={{ marginTop: "10px" }}
                  />
                </div>
              );
            })}
            <Button
              onClick={addSchedule}
              className={classes.button}
              color="primary"
            >
              Add Schedule
            </Button>
          </>
        );
      case 5:
        return (
          <>
            {doors.map((door, index) => {
              return (
                <>
                  <div style={{ display: "flex", alignItems: "baseline" }}>
                    <p style={{ marginRight: "10px" }}>{door.doorName}</p>
                    <FormControl>
                      <InputLabel id="group-id">Group</InputLabel>

                      <Select
                        labelId="group-id"
                        value={door.group}
                        onChange={e => onHandleDoorGroupChange(e, index)}
                        style={{ width: "10rem", marginRight: "10px" }}
                      >
                        {groups.map(group => (
                          <MenuItem value={group}>{group}</MenuItem>
                        ))}
                      </Select>
                    </FormControl>

                    <FormControl>
                      <InputLabel id="schedule-id">Schedule</InputLabel>

                      <Select
                        labelId="schedule-id"
                        value={door.schedule}
                        onChange={e => onHandleDoorScheduleChange(e, index)}
                        style={{ width: "10rem", marginRight: "10px" }}
                      >
                        {schedules.map(schedule => (
                          <MenuItem
                            value={
                              schedule.startTime +
                              "-" +
                              schedule.endTime +
                              " " +
                              schedule.days
                            }
                          >
                            {schedule.startTime +
                              "-" +
                              schedule.endTime +
                              " " +
                              schedule.days}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                </>
              );
            })}
          </>
        );
      case 6:
        return (
          <>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Door</TableCell>
                  <TableCell align="right">Control Panel</TableCell>
                  <TableCell align="right">Site</TableCell>
                  <TableCell align="right">Group</TableCell>
                  <TableCell align="right">Schedule</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {doors.map(door => (
                  <TableRow key={door.doorName}>
                    <TableCell component="th" scope="row">
                      {door.doorName}
                    </TableCell>
                    <TableCell align="right">{door.controlPanel}</TableCell>
                    <TableCell align="right">{door.site}</TableCell>
                    <TableCell align="right">{door.group}</TableCell>
                    <TableCell align="right">{door.schedule}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </>
        );
      default:
        return "Unknown step";
    }
  };

  return (
    <div className={classes.root}>
      <Typography variant="h6">
        New Customer Onair Account
      </Typography>

      <Stepper
        activeStep={activeStep}
        orientation="vertical"
        classes={{
          palette: {
            primary: "red"
          }
        }}
      >
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              {getStepContent(index)}

              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                    color="primary"
                    variant="contained"
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? "Finish" : "Next"}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>All steps completed - you&apos;re finished</Typography>
          <Button onClick={handleReset} className={classes.button}>
            Reset
          </Button>
        </Paper>
      )}
    </div>
  );
}
